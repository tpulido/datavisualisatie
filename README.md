Technologie in publieke ruimtes
=================

Door T. Pulido

26 september 2017

Inhoudsopgave
=================
* [Inleiding](#inleiding)
* [Hypothese](#hypothese)
* [Onderzoek](#onderzoek)
    * [Cameragebieden](#cameragebieden)
    * [Misdaadcijfers](#misdaadcijfers)
    * [Oppervlaktes](#oppervlaktes)
    * [Datasets](#datasets)
* [Proces](#proces)
    * [Mogelijke onderwerpen](#Mogelijke onderwerpen)   
    * [Hypotheses](#hypotheses)   
    * [Zoekwoorden](#zoekwoorden)
    * [Buurtonderzoek](#buurtonderzoek)
* [Bronnen](#bronnen)

Inleiding
=================

In dit document laat ik mijn onderzoek zien over **technologie in publieke ruimtes**.
Ik wil informatie verzamelen over beveilingcamera's en misdaadcijfers om hier correlaties tussen te vinden.

Hypothese
=================

“In een gebied waar actief cameratoezicht wordt ingezet, is de criminaliteit lager dan in gebieden zonder camera’s”

**Doelgroep**
Wat vinden mensen op straat over dit onderwerp?
 
**Onderwerp**
Zit er correlatie tussen gebieden waar actief cameratoezicht wordt ingezet en gebieden waar dit niet wordt gedaan?
- Wat blijkt er uit de cijfers van geweldsincidenten en vermogensmisdrijven?
- Geweldsmisdrijf, vermogensmisdrijf per 1000 inwoners van een specifieke locatie?
- Gemiddelde geweldsmisdrijven van Nederland en de Randstad nemen, deze vergelijken met het gemiddelde van de specifieke locatie

Onderzoek
=================

### Cameragebieden

Om correlaties tussen verschillende gebieden weer te geven heb ik een lijst gemaakt van bekende gebieden waar actief toezicht gehouden wordt.
Deze lijst heb ik opgesteld met behulp van bronnen. De meeste bronnen zijn evaluaties van cameratoezicht in bepaalde gebieden. In deze evaluaties heb ik namen van wijken, gebieden en stadsdelen gevonden.
Op basis van deze informatie baseer ik dat er in deze gebieden actief cameratoezicht is.

Hieronder volgt de lijst met verwijzing naar de bron (deze bronnen zijn ook in de bronnenlijst te vinden).

**Amsterdam ([Cameragebieden Amsterdam](https://maps.amsterdam.nl/overlast/))**
* Osdorp
* Diamantbuurt
* Indische buurt (West en Oost)
* Holendrecht / Reigersbos
* Oude Pijp
* Burgwallen-Oude Zijde
* Burgwallen-Nieuwe Zijde


**Utrecht ([Bron 1](https://utrecht.dataplatform.nl/dataset/fa374238-7b29-4e8e-ab20-4e242a143085), [Bron 2](https://utrecht.dataplatform.nl/resource/77acfd96-c904-4e45-81bf-12d2e7330a81), [Bron 3](https://www.utrecht.nl/fileadmin/uploads/documenten/wonen-en-leven/veiligheid/2016-Uitvoeringsplan-Cameratoezicht.pdf))**
* Galgenwaard en omgeving
* Oude binnenstad
* Nieuw Hoograven-Zuid
* Kanaleneiland-Noord
* Mariaplaats en omeving
* Ondiep


**Rotterdam ([Straten waarin cameratoezicht is, per deelgemeente](https://www.publicspaceinfo.nl/media/uploads/files/ROTTERDAM_2011_0002.pdf)**
* Kralingen-Crooswijk
* Centrum
* Oude Westen
* Delfshaven
* Charlois
* Feijenoord


### Misdaadcijfers

Het CBS heeft een grote dataset met alle soorten criminaliteit per gemeente. Met het gebruik van deze data set heb ik gegevens onder
elkaar kunnen zetten. Hier kon vervolgens mee rekenen om bijvoorbeeld de geweldsmisdrijven per 1000 inwoners weer te geven.

De misdaadcijfers per gemeente is veruit mijn belangrijkste bron geweest. Ik kon hier per wijk de cijfers bekijken van elk misdaad type.

[Geregistreerde criminaliteit per gemeente, wijk en buurt, 2010-2015](https://www.cbs.nl/nl-nl/maatwerk/2016/45/geregistreerde-criminaliteit-per-gemeente-wijk-en-buurt-2010-2015)

Ik heb ervoor gekozen om mij te richten op twee soorten criminaliteit, namelijk:
* Geweldsmisdrijven
* Vermogensdelicten (diefstal)

Ik heb voor deze twee gekozen omdat dit verschillende soorten criminaliteit zijn. Bij uitgaansgebieden heb je meer vechtpartijen en bij toeristsche gebieden meer vermogensdelicten. Ik ben dan benieuwd of er een correlatie tussen de twee te vinden is.

### Oppervlaktes

Om een duidelijk beeld te kunnen geven over bepaalde gebieden vond ik dat het nodig was om weer te geven hoe groot deze gebieden zijn. 
Ik heb geprobeerd om dit via twee manieren te doen, namelijk:

* Inwoners per gebied
* Oppervlaktes in km²

Door deze twee variablen is het eenvoudig om in te beelden over wat voor een groot gebied de cijfers gaan.
Heeft een gebied veel inwoners maar weinig vierkante oppervlakte? Dan is dit een dicht bevolkt gebied.
Is het gebied erg groot qua vierkante oppervlakte maar heeft het weinig inwoners?
Dan weet je dat dit een erg groot gebied is of dat het een industrieel gebied is.

Zie bronnenlijst voor verwijzing naar waar ik deze informatie gevonden heb.

### Datasets

In mijn visualisatie van alle data heb ik geprobeerd om inzichtelijk te maken of gebieden met actieve camerabewaking nou echt veiliger zijn dan gebieden waar dit niet gebeurt.
Ik heb geprobeerd om grote, kleine, dun en dicht bevolkte gebieden met elkaar te vergelijken. 

Doormiddel van de volgende variablen heb ik overzicht proberen te creëren:
* Actief camera toezicht? (Ja of nee, hiermee vergelijk ik gebieden met en zonder toezicht)
* Aantal inwoners ()
* Oppervlakte in vierkante km²
* Aantal geweldsmisdrijven
* Aantal diefstallen
* Geweldsmisdrijven per 1000 inwoners
* Diefstallen per 1000 inwoners
* Hoeveel procent boven het gemiddelde van de randstad (van geweldsmisdrijven en diefstal)

Hieronder staat de link naar mijn verwerkte data:

[Google Sheet met datavisualisatie](https://docs.google.com/spreadsheets/d/1hfCRthan4UPPRbFv8r7lHGjUflvA6CVztFDXLL7mteM/edit?usp=sharing)   


Proces
=================
Hieronder wordt op een chronologische manier laten zien hoe ik dit onderzoek heb aangepakt.
Ook is te zien dat ik vaker van richting van veranderd.

### Mogelijke onderwerpen
Dit zijn de onderwerpen waar ik mijn onderzoek mee ben begonnen. 

- **Beveiligingscamera’s?**
    - Privacy
    - **Misdaad**
- Door alle technologie in publieke ruimtes, worden dingen hackable
- Drones

Ik heb gekozen voor beveiligingscamera’s en misdaad. Uit dit onderzoek moet blijken of gebieden met cameratoezicht veiliger zijn dan anderen. 

### Hypotheses
Dit zijn de eerder gestelde hypotheses. Deze zijn bij de voortgang van het onderzoek veranderd.
* “De toename van beveiligingscamera’s in de stad is een positieve vooruitgang” 
* “De toename van beveiligingscamera’s wordt door de bewoners als positief ervaren” 
* “Cameras zorgen ervoor dat criminaliteit (tijdelijk) daalt
* “Door de toename van beveiligingscamera’s is de criminaliteit in de binnenstad verminderd”
* “Hoe meer beveiligingscamera’s, hoe meer de criminaliteit in de binnenstad verminderd
* **“In een gebied waar actief cameratoezicht wordt ingezet, is de criminaliteit lager dan in gebieden zonder camera’s”**


### Zoekwoorden
Ik heb geprobeerd om mijn zoekwoorden breed te houden. Naarmate mijn onderwerp zich ontwikkelde ben ik locatie gericht gaan zoeken.
- Camera effectivity
- effectivity surveillance
- effectivity cameras toezicht effectiviteit
- Effectiviteit cameratoezicht
- Meningen cameratoezicht
- Aanslagen camera's
- Camera's en maatschappij
- Maatschappij en privacy
- Daling criminaliteit door camera's
- Cameragebieden cijfers
- Cameragebieden Amsterdam
- CBS Cameragebieden
- CBS Cameratoezicht
- Misdaadcijfers CBS
- Criminaliteitscijfers per gemeente
- Geweldsincidenten Amsterdam
- Cameragebieden Utrecht
- Cameragebieden Rotterdam
- Cameratoezicht Rotterdam + Utrecht


**Operatoren en filters**

* site:https://cbs.nl/
* criminaliteit + gemeente
* filetype:PDF Cameratoezicht OR Cameragebieden
* Secured areas + Amsterdam OR Rotterdam
* link:https://cbs.nl/ cameragebieden
* link:https://utrecht.dataplatform.nl/ camera's
* filteren op nieuwsberichten
* filteren op artikel
* filteren op publicatie


### Buurtonderzoek 

Voor dit buurtonderzoek heb ik gebruik gemaakt van enquêtes. Ik heb tot een bepaalde hoogte inzichtelijk kunnen maken hoe mensen in Amsterdam denken over cameratoezicht in verband met hun privacy.
Deze enquête is gemaakt in een vroeg stadium van het onderzoek waardoor niet alle verzamelde data bijdraagt aan de uiteindelijke visualisatie.
Wel denk ik dat het buurtonderzoek bijdraagt aan de totaliteit van dit onderzoek omdat het de mening van de mensen reflecteert.

Enquête afgenomen op : 05-09-2017

Afgenomen in : het centrum van Amsterdam

Aantal afgenomen enquêtes : 25

Enquete vragen :
* 1. Geven beveiligingscamera's u een veilig gevoel?
* 2. Wat vind u van de stelling: "Beveiligingscamera’s zorgen ervoor dat de straten veiliger worden"?
* 3. Vind u camera's op straat gevaarlijk voor uw privacy?
* 4. Neemt u maatregelen om uw privacy te waarborgen?


Resultaten : 

![1.](https://gitlab.com/tpulido/datavisualisatie/raw/master/1.veilig-gevoel.png)

![2.](https://gitlab.com/tpulido/datavisualisatie/raw/master/2.straten-veiliger.png)

![3.](https://gitlab.com/tpulido/datavisualisatie/raw/master/3.gevaarlijk-voor-privacy.png)

![4.](https://gitlab.com/tpulido/datavisualisatie/raw/master/4.maatregelen.png)



Bronnen
=================

**(1)** - In Nederland is het gebruik van beveiligingscamera’s in de stad voor opsporingsdoeleinden een voorbeeld van function creep.
Ook op het terrein van justitiële interventies, straffen en maatregelen bestaat de neiging om wat werkt voor een bepaalde categorie ook maar toe te passen op andere gevallen, dit in weerwil van strenge What Works-richtlijnen hieromtrent.
Denk ook aan de HALT-maatregel, oorspronkelijk bedoeld voor jongeren die voor het eerst waren opgepakt voor vandalisme, maar die na verloop van tijd ook aan zwaardere gevallen werd opgelegd. 
De neiging om een maatregel bedoeld als oplossing voor een bepaald probleem ook toe te passen op een ander probleem – zelfs als niet vaststaat dat het gekozen middel überhaupt werkt – is heel sterk in politieke en beleidskringen.

https://search-proquest-com.rps.hva.nl:2443/docview/917328556/fulltextPDF/2281BC8FC4A04C3CPQ/1?accountid=130632

Controle bron: 

De bron komt uit het voorwoord van een publicatie van het WODC (Wetenschappelijk Onderzoek- en Documentatiecentrum) over Justitiële Verkenningen.
Het WODC is onderdeel van het ministerie van veiligheid en justitie waardoor ik denk dat deze bron betrouwbaar is.


**(2)** - Insecam kan die beelden laten zien omdat beheerders van beveiligingscamera's het standaardwachtwoord vaak niet blijken aan te passen. Standaard zijn dat wachtwoorden als 'admin:admin' of 'admin:12345'.
Als die wachtwoorden niet veranderd worden, zijn de beelden via de site live te bekijken. Het gaat om camera's die in openbare ruimten, winkels, horeca en zelfs woningen hangen. Bij de beelden wordt de plaatsnaam van de camera getoond, en in veel gevallen zelfs de precieze gps-locatie.

http://www.nu.nl/internet/3919779/beelden-duizenden-beveiligingscameras-onbedoeld-openbaar.html

Controle bron: 

Nu.nl heeft Webwereld.nl als bron gebruikt. Webwereld.nl is een zakelijke IT-nieuwssite waar vooral over nieuwe ontwikkelingen wordt gepraat.
Zij hebben het in dit artikel over een andere website (http://insecam.com/) waar openbare websites live draaien, dit staat nog steeds online waardoor dit mij een betrouwbare bron lijkt.

**(3)** - VPRO Thema: Wat nou privacy? 1/5

Bestaat er anno 2010 nog zoiets als privacy? De intiemste zaken over onszelf delen we tegenwoordig op blogs, op Facebook of op Hyves. Maar ook overheidsdiensten en commerciële bedrijven verzamelen intussen steeds meer van onze gegevens.

https://www.youtube.com/watch?v=ZfzGigg3ldw

Controle bron: 

Dit is een video van de VPRO. De VPRO maakt vaker onderzoeksvideo's waar ze met zoveel mogelijk specialisten aan de praat gaan. Bij dit filmpje is dit ook het geval waardoor ik denk deze bron betrouwbaar is. 


**(4)** - Een onderzoek naar het recht op privacy bij het inzetten van cameratoezicht door middel van drones in de gemeente Utrecht

Dit onderzoeksrapport is geschreven voor de gemeente Utrecht. De gemeente Utrecht wil drones inzetten zodat zij aanhoudende en verplaatsende overlast in de publieke ruimte beter en effectiever kunnen handhaven. Sinds 1 juli 2016 heeft er een wijziging plaatsgevonden in artikel 151c Gemeentewet wat het mogelijk maakt om flexibel cameratoezicht in te zetten. Hierdoor heeft per 1 juli 2016 de burgemeester de bevoegdheid om drones in te zetten bij het aanpakken van verplaatsende overlast en criminaliteit.

https://hbo-kennisbank.nl/record/sharekit_hu/oai:surfsharekit.nl:f376ecce-8171-4697-ada4-2c487fdb1a58

Controle bron: 

Dit onderzoek is gedaan als afstudeerscriptie. Hierdoor zijn alle bronnen in dit onderzoek nauwkeurig geverifieerd. Hierdoor denk ik dat dit een betrouwbare bron is.


**(5)** - Een onderzoek naar de waarborging van grondrechten bij het houden van cameratoezicht in de Gemeente Terneuzen

https://hbo-kennisbank.nl/record/sharekit_jhs/oai:surfsharekit.nl:a2bf5734-800e-420b-b50b-228ddead50d5

Controle bron: 

Net als de vorige bron is dit een afstudeerscriptie. Ook hier zijn alle bronnen nauwkeurig geverifieerd waardoor ik vertrouwen heb in de betrouwbaarheid van de informatie.


**(6)** - Camera Surveillance within the UK: Enhancing Public Safety or a Social Threat?

“It is timely to consider how the proliferation of surveillance cameras developed and what evidence there is to justice their place within society. Do they significantly contribute to public safety and prevention of crime and terrorist activity, or are they becoming a blot on the landscape, unnecessarily interfering with private lives?”

https://search-proquest-com.rps.hva.nl:2443/docview/919899349/F5BBCE17E3054F01PQ/5?accountid=130632

Controle bron: 

Dit is een publicatie van Sheldon Barrie in een wetenschappelijk tijdschrift. 
Hij heeft boeken geschreven als: Policing and Criminal Justice, Policing and Technology”, hierdoor denk ik dat Sheldon veel over dit onderwerp weet en geen onjuiste informatie wil verspreiden omdat hier ook andere critische specialisten naar kijken.


**(7)** - Privacy

Na de aanslagen op 11 september 2001 zijn vaker verscherpte maatregelen getroffen om de veiligheid te waarborgen. Dat gaat vaak gepaard met een verminderde privacy.

http://adbank.dossiers.knipselkranten.nl/privacy

Dit document geeft weer dat er veranderingen in veiligheid hebben plaatsgevonden. Een van deze gevolgen is dat er meer toezichtcamera's bij zijn gekomen. Omdat het artikel in 2017 geplaatst is en enkel veranderingen aan het licht brengt, vind ik dit een betrouwbare bron.


**(8)** - Privacy in the Digital Age

Google's cameras have captured people going into porn shops, for example. In the U.S., it hasn't yet stirred up much fuss, however, beyond a recent lawsuit in which a Pittsburgh couple sued Google for invasion of privacy.

https://www-warc-com.rps.hva.nl:2443/SubscriberContent/Article/privacy_in_the_digital_age/90401

Controle bron: 

Deze publicatie komt uit 2008 maar gaat in op onderwerpen die nu nog steeds spelen. De bron is dus niet helemaal actueel maar wel betrouwbaar omdat deze onderwerpen nog steeds relevant zijn.

**(9)** - Evaluatie cameratoezicht op openbare plaatsen - (PDF)

https://www.google.nl/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=0ahUKEwilrODOu5_WAhWGUlAKHUdEAeoQFggzMAI&url=https%3A%2F%2Fwww.tweedekamer.nl%2Fdownloads%2Fdocument%3Fid%3D24b8fbd8-cb26-4111-b9f2-162822d397c1%26title%3Dtweemeting.pdf&usg=AFQjCNEdqphApW8EZm5Fgqs-KL9bul0yMA 

Controle bron: 

"Regioplan is een onafhankelijke, betrouwbare en effectieve partner in het oplossen van beleidsvraagstukken van overheden, het maatschappelijk middenveld en het bedrijfsleven. Wij leveren beleidsonderzoek, beleidsadvies en (personele) ondersteuning bij de implementatie van beleid."
Omdat regioplan een onafhankelijke organisatie is en dit onderzoek gaat over effectiviteit dus vooral cijfers laat zien, denk ik dat de informatie betrouwbaar is. Wel komt dit document uit 2007 waardoor alle informatie niet actueel is.
Wel laat het zien hoe effectief cameratoezicht bleek te zijn in die tijd, daarom denk ik dat deze bron nog steeds belangrijke informatie bevat.


**(10)** - Vijf dingen die u moet weten over het cameratoezicht in Utrecht

Voor het beheer en onderhoud van de camera’s is in 2014 een bedrag gemoeid van 338.068 euro. Deze kosten zullen door naderende afschrijvingsdata elk jaar toenemen. In 2022 wordt het definitieve jaarlijkse bedrag bereikt van 476.323 euro.

https://www.duic.nl/algemeen/vijf-dingen-die-u-moet-weten-over-het-cameratoezicht-in-utrecht/

Controle bron: 

In dit nieuwsstuk gaat het over de kosten van cameratoezicht. Het is niet helemaal actueel omdat het uit 2014 komt, wel hebben ze het hier over geldbedragen en speculeren zij over toekomstige uitgaven omtrent cameratoezicht. Hierdoor denk ik dat deze bron betrouwbaar is. 
Duic is: "Utrechts eigen, innovatieve nieuwsplatform met dagelijks algemeen, cultureel en gemeentelijk nieuws uit de stad Utrecht".

**(11)** - Evaluatie cameratoezicht Utrecht

Er zijn twee soorten incidenten die worden geregistreerd in de toezichtcentrale: eigen waarnemingen en verzoeken om mee te kijken. De eerste groep die we hier behandelen zijn de eigen waarnemingen, dus de incidenten die zonder cameratoezicht niet zouden zijn waargenomen. In de eerste acht maanden van 2013 zijn 1.616 incidenten waargenomen door de toezichtcentrale. Dit komt neer op circa zeven incidenten per dag. Het blijkt dat een kwart (23%) wildplassen betreft. Vernieling/baldadigheid (14%), overlast door alcohol/drugs (12%) en verdachte situaties (12%) komen ook relatief vaak voor

https://www.dsp-groep.nl/wp-content/uploads/18pecamutrecht_Evaluatie_cameratoezicht_Utrecht_2014.pdf

Controle bron: 

Dit rapport is in opdracht gemaakt van de gemeente Utrecht door DSP (een bureau voor beleidsadvies, onderzoek en management). Deze evaluatie komt uit 2013 maar gaat in op cameragebieden, misdaadcijfers en de effectiviteit van de camera plaatsing. 
Deze evaluatie is naar mijn idee betrouwbaar omdat het gaat om cijfers en meningen van bewoners. Ook baseert de gemeente Utrecht hierop zijn beleid waardoor het rapport nauwkeurig en valide moet zijn.


**(12)** - Zeker 200.000 camera's houden je in de gaten

Het lijkt een simpele vraag. Hoeveel beveiligingscamera's hangen er in Nederland in de openbare ruimte?  Ook zijn er van veel locaties helemaal geen gegevens beschikbaar. Er hangen in Nederland dus in ieder geval 204.441 camera's, maar het zijn er sowieso meer. Dimitri Tokmetzis van Sargasso: "Als ik mij mag wagen aan een educated guess dan zeg ik dat we tegen een miljoen camera's aanzitten in Nederland en wellicht zijn we daar al overheen."

https://nos.nl/artikel/489384-zeker-200-000-camera-s-houden-je-in-de-gaten.html

Controle bron: 

Het gaat in dit nieuwsartikel over een schatting naar de hoeveelheid beveiligingscamera’s in Nederland. Zij hebben deze informatie van een onderzoek dat Sargasso gedaan heeft.
Sargasso is een Nederlands weblog dat onder andere bekend werd nadat het de hele ontwerp-EU-grondwet had doorgespit om de kiezer te informeren. 
Het totaal aantal camera's in Nederland is en blijft een schatting waardoor ik niets wil baseren op deze bron, het biedt vorm in de richting van dit onderzoek.

**(13)** - Bekendmakingen cameratoezicht

De burgemeester heeft besloten om per 13 juli 2017 camera's te plaatsen in Gein. De begrenzing van het gebied waar de camera's worden ingezet is aangegeven in het besluit. Dit cameraproject loopt tot en met 31 december 2017

https://www.amsterdam.nl/wonen-leefomgeving/veiligheid/bekendmakingen/

Controle bron: 

In deze bron wordt weergegeven wanneer en waarom de gemeente van Amsterdam beslist om cameratoezicht te plaatsen in bepaalde straten. Deze bron is naar mijn idee valide en betrouwbaar omdat de gemeente Amsterdam hier zijn inwoners mee informeert.

**(14)** - Cameratoezicht Evaluatie Centrum en Woensel West (Eindhoven)

Ongeveer 28% van de Nederlandse gemeenten zet cameratoezicht op openbare plaatsen in. Uit een groot deel van de onderzoeksrapporten komt naar voren dat cameratoezicht maakt dat de mensen zich veiliger voelen. b Minder helder is of door cameratoezicht het aantal delicten dat gepleegd wordt afneemt. Er zijn aanwijzingen dat cameratoezicht wel invloed heeft op vermogensdelicten, maar niet zo zeer op geweldsdelicten.

https://eindhoven.raadsinformatie.nl/document/4288657/1/Cameratoezicht_Evaluatie_2012_Centrum_en_Woensel_West

Controle bron: 

Dit is een evaluatie van het cameratoezicht in Woensel West. Er wordt hier doormiddel van cijfers laten zien of cameratoezicht succesvol is geweest. Naar mijn idee is deze bron betrouwbaar omdat het onderzoek gebruik maakt van cijfers van het CBS.
Ook is dit onderzoek gedaan door de gemeente Eindhoven. De gemeente creëert vaak beleid op basis van dit soort evaluaties.


**(15)** - 'Meer beveiligingscamera's aangemeld bij politie'

Trouw schrijft dat er inmiddels 160.000 beveiligingscamera's bekend zijn bij de politie, een toename van 60.000 vergeleken met oktober vorig jaar.

https://nos.nl/artikel/2191274-meer-beveiligingscamera-s-aangemeld-bij-politie.html

https://www.trouw.nl/home/de-particuliere-camera-rukt-op-als-hulpje-van-politie~a7139e4d/

In deze bronnen wordt een groei weergeven in het aantal particuliere beveiligingscamera’s. Deze bron is naar mijn idee betrouwbaar omdat zowel de NOS als trouw hier over bericht geeft.
Omdat ik de genoemde cijfers van deze stukken niet kan vinden kan ik niet controleren waar deze vandaan komen.

**(16)** - 2016 Uitvoeringsplan Cameratoezicht

In dit stuk staat centraal hoe de effectiviteit van de huidige camera’s verbeterd wordt op basis van een analyse in de wijken, een analyse van cijfers en registraties van politie. Verder staat een aantal operationele verbeteringen opgenomen

https://www.utrecht.nl/fileadmin/uploads/documenten/wonen-en-leven/veiligheid/2016-Uitvoeringsplan-Cameratoezicht.pdf

Controle bron:
Deze bron is naar mijn idee valide omdat het onderzoek gedaan is door de gemeente Utrecht. Gemeentes baseren hun beleid hierop waardoor de informatie moet kloppen.
Ook behandelen ze in dit onderzoek criminaliteitscijfers. Deze komen overeen met de cijfers van het CBS waardoor ik denk dat de rest van deze informatie ook kloppend moet zijn.


**(17)** - Evaluatie cameratoezicht op openbare plaatsen

Dit is evaluatie over cameratoezicht gemaakt door Regioplan.

"Regioplan is een onafhankelijke, betrouwbare en effectieve partner in het oplossen van beleidsvraagstukken van overheden, het maatschappelijk middenveld en het bedrijfsleven. Wij leveren beleidsonderzoek, beleidsadvies en (personele) ondersteuning bij de implementatie van beleid."

https://www.politieacademie.nl/kennisenonderzoek/kennis/mediatheek/PDF/78158.pdf

Controle bron:

Deze bron is naar mijn idee valide en betrouwbaar omdat zij gebruiken maken van een reeks andere bronnen. Deze bronnen bestaan allemaal uit onderzoeken van gemeentes. Dit is als het ware een collectie van alle cijfers die uit de onderzoeken komen (zie literatuur lijst op pagina 37 t/m 39).


**(18)** - Interactieve kaart

Een interactieve kaart waar de actieve camera gebieden van Amsterdam getoond worden.
https://maps.amsterdam.nl/open_geodata/

https://maps.amsterdam.nl/overlast/

**Controle bron volgens Sanne Blauw:**

**Uit welke dataset komt het cijfer tevoorschijn?**

Dit weet ik niet. Dit is de enige dataset waar ik deze informatie uit kan vinden.

*Ontbreekt belangrijke informatie bij het cijfer?*

Ja, ik zou graag weten hoeveel camera's er in deze gebieden hangen. Een precies aantal hoeft niet maar een inzicht hierin zou het onderzoek verduidelijken.

**Is het cijfer logisch?**

Nee, soms niet. Je zou denken dat er in beruchte gebieden (bijvoorbeeld de Bijlmer) ook actief cameratoezicht is. Echter staan deze niet op de kaart vermeld wat ik best raar vind.


**(19)**  - Geregistreerde criminaliteit per gemeente, wijk en buurt, 2010-2015

https://www.cbs.nl/nl-nl/maatwerk/2016/45/geregistreerde-criminaliteit-per-gemeente-wijk-en-buurt-2010-2015

**Controle bron volgens Sanne Blauw:**

Ik denk dat deze bron valide en betrouwbaar is omdat alle data van het CBS komt. 
Zo zegt het CBS zelf: "CBS heeft als missie het publiceren van betrouwbare en samenhangende statistische informatie, die inspeelt op de behoefte van de samenleving. Deze missie vereist dat de kwaliteit van de statistische informatie gegarandeerd is. CBS heeft daartoe een systeem van kwaliteitszorg ingericht, dat gebaseerd is op de hoogste Internationale normen"

**Uit welke dataset komt het cijfer tevoorschijn?**

Alle data komt uit een dataset van het CBS

*Ontbreekt belangrijke informatie bij het cijfer?*

Naar mijn idee is de data compleet. Wel zouden ze  oppervlaktes in vierkante kilomers kunnen vermelden om beter inzichtelijk te maken hoe groot een bepaald gebied is.

**Is het cijfer logisch?**

Op het eerste gezicht lijken alle cijfers logisch. Tot dat je gekke sprongen ziet in criminaliteit (bijvoorbeeld het Westelijk Havengebied).
Alhoewel dit cijfer mischien niet helemaal logisch is, toch denk ik dat het niet onjuist is. Sommige gebieden zijn heel klein maar hebben toch veel criminaliteit. Dit kan liggen aan verschillende omstandigheden. Daarom denk ik dat deze cijfers uiteindelijk wel correct zijn.


**(20)** - Cameratoezicht Utrecht

Lijsten met locaties in Utrecht waar camera toezicht actief wordt ingezet

https://utrecht.dataplatform.nl/dataset/fa374238-7b29-4e8e-ab20-4e242a143085

https://utrecht.dataplatform.nl/resource/77acfd96-c904-4e45-81bf-12d2e7330a81

**Controle bron volgens Sanne Blauw:**

**Uit welke dataset komt het cijfer tevoorschijn?**

Zover ik weet is dit de dataset. Ik heb veel gezocht naar meer informatie over Utrecht, maar helaas heb ik niet veel meer kunnen vinden.

*Ontbreekt belangrijke informatie bij het cijfer?*

Ja, ik zou graag weten hoeveel camera's er in deze gebieden zijn. Ook zou het handig zijn om te deze informatie in een interactieve kaart te zetten (zoals Amsterdam dat doet).
Zo kunnen onderzoekrs of bewoners de data op een overzichtelijkere manier bekijken.

**Wie brengt het cijfer?**

Ik denk dat deze informatie valide en betrouwbaar is omdat alle data van de gemeente Utrecht komt. 
Zij hebben er geen baad bij om misleidende informatie te geven. Ook creëren zij beleid op basis van dit soort cijfers.


**(21)** - Afbeelding met cameragebieden in Utrecht

http://destadutrecht.nl/misdaad/cameratoezicht-kost-jaarlijks-ruim-8-ton/

Controle bron:

Deze afbeelding is afkomstig uit bron (18). Ik wil deze afbeelding ook apart uitlichten omdat mijn onderzoek deze afbeelding gebruikt.
Ik denk dat deze bron valide en betrouwbaar is omdat de informatie van de gemeente Utrecht komt. 


**(22)** - Straten waarin cameratoezicht is (per deelgemeente)

Lijst met locaties in Rotterdam waar camera toezicht actief wordt ingezet
https://www.publicspaceinfo.nl/media/uploads/files/ROTTERDAM_2011_0002.pdf

Controle bron:

Deze bron is afkomstig van Public Space Info. PCI zegt dit over zichzelf: "Onafhankelijke informatie en NIEUWS over leefomgeving, duurzaamheid en openbare ruimte".
In dit document worden de huidige gebieden van het cameratoezicht genoemd. Een deel van mijn onderzoek gebruikt deze informatie. 

**(23)** - Oppervlaktes van gebieden

In mijn samengestelde data set laat ik zien hoe groot een bepaald gebied is, dit geef ik weer doormiddel van de oppervlakte van een gebied.
De oppervlaktes van de verschillende plekken komen van Funda, Wikipedia en Drimble. Hieronder staan alle bronnen.

https://drimble.nl/buurten/3630381/osdorp-oost.html

https://drimble.nl/buurten/3630382/osdorp-midden.html

https://nl.wikipedia.org/wiki/Diamantbuurt_(Amsterdam)

https://drimble.nl/buurten/3630532/indische-buurt-oost.html

https://drimble.nl/buurten/3630531/indische-buurt-west.html

https://drimble.nl/buurten/3630796/holendrecht-reigersbos.html

https://nl.wikipedia.org/wiki/Oude_Pijp

https://www.funda.nl/buurtinfo/amsterdam/burgwallen-oude-zijde/kenmerken/?ref=85193795

https://drimble.nl/buurten/3630001/burgwallen-nieuwe-zijde.html

https://drimble.nl/buurten/3630006/jordaan.html

https://drimble.nl/buurten/3630110/westelijk-havengebied.html

https://nl.wikipedia.org/wiki/Nieuwe_Pijp

https://nl.wikipedia.org/wiki/Diemen

https://drimble.nl/buurten/3440535/galgenwaard-en-omgeving.html

https://nl.wikipedia.org/wiki/Binnenstad_(Utrecht)

https://drimble.nl/buurten/3440733/nieuw-hoograven-zuid.html

https://drimble.nl/buurten/3440832/kanaleneiland-noord.html

https://drimble.nl/buurten/3440612/lange-elisabethstraat-mariaplaats-en-omgeving.html

https://drimble.nl/buurten/3440222/ondiep.html

https://drimble.nl/buurten/3440823/transwijk-noord.html

https://drimble.nl/buurten/3440811/dichterswijk.html

https://drimble.nl/buurten/3440626/nieuwegracht-oost.html

https://drimble.nl/buurten/3440121/lombok-oost.html

https://drimble.nl/buurten/3440123/lombok-west.html

https://drimble.nl/buurten/3440212/pijlsweerd-noord.html

https://drimble.nl/buurten/3440211/pijlsweerd-zuid.html

https://drimble.nl/wijken/59908/wijk-08-kralingen-crooswijk.html

https://nl.wikipedia.org/wiki/Rotterdam_Centrum

https://drimble.nl/buurten/5990111/oude-westen.html

https://drimble.nl/buurten/5990320/delfshaven.html

https://drimble.nl/wijken/59915/wijk-15-charlois.html

https://drimble.nl/wijken/59910/wijk-10-feijenoord.html

https://drimble.nl/buurten/5990814/rubroek.html

https://drimble.nl/buurten/5990516/provenierswijk.html

https://drimble.nl/buurten/5990515/agniesebuurt.html

https://drimble.nl/buurten/5990110/stadsdriehoek.html

Controle bron:

Op Funda kun je een woning vinden. Daarom heeft Funda er baad bij om correcte informatie te geven over bepaalde gebieden. Als zij onjuiste informatie zouden geven, zouden mensen Funda als verkoop site ontwijken.

"Drimble verzamelt alle beschikbare informatie per postcode, plaats, gemeente, regio en provincie" - drimble.nl.
Omdat Drimble een website is die toegewijd is aan informatie over locaties, twijfel ik niet aan de betrouwbaarheid.

Wikipedia is vaak geen goede bron omdat iedereen hier iets in aan kan passen. Als het om gebieden gaat is deze informatie meestal betrouwbaar en valide.
Dit is informatie waar de gemeente over beschikt en niet erg vind om te delen, daarom twijfel ik niet aan de betrouwbaarheid van deze gegevens.

